package ru.fitsme.android.presentation.fragments.signinup.events;

public interface SignInUpBindingEvents {
    void onClickSignUp();

    void onClickSignIn();
}
