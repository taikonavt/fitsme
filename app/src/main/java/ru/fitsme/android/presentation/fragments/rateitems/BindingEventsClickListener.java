package ru.fitsme.android.presentation.fragments.rateitems;

public interface BindingEventsClickListener {
    void onClickLikeItem();
    void onClickRefresh();
    void onClickDislikeItem();
    void onClickFilter();
}
