package ru.fitsme.android.presentation.main;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

@Singleton
public class AuthNavigation {
    public static final String NAV_SIGN_IN_UP = "SignInUp";
    public static final String NAV_SIGN_UP = "SignUp";
    public static final String NAV_SIGN_IN = "SignIn";
    public static final String NAV_MAIN_ITEM = "MainItem";
    public static final String NAV_SPLASH = "Splash";

    private Cicerone<Router> cicerone;

    @Inject
    public AuthNavigation() {
        cicerone = Cicerone.create();
    }

    private NavigatorHolder getNavigatorHolder() {
        return cicerone.getNavigatorHolder();
    }

    private Router getRouter() {
        return cicerone.getRouter();
    }

    private void goNavigate(String navigationKey) {
        getRouter().navigateTo(navigationKey);
    }

    public void setNavigator(Navigator navigator) {
        getNavigatorHolder().setNavigator(navigator);
    }

    public void removeNavigator() {
        getNavigatorHolder().removeNavigator();
    }

    public void goSignInUp() {
        getRouter().newRootScreen(NAV_SIGN_IN_UP);
    }

    public void goSignIn() {
        goNavigate(NAV_SIGN_IN);
    }

    public void goSignUp() {
        goNavigate(NAV_SIGN_UP);
    }

    public void goToMainItem() {
        getRouter().newRootScreen(NAV_MAIN_ITEM);
    }

    public void goToSplash(){
        goNavigate(NAV_SPLASH);
    }
}
