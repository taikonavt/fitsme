package ru.fitsme.android.presentation.fragments.signinup.events;

public interface SignInBindingEvents {
    void onClickSignIn();
}
