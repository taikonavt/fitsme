package ru.fitsme.android.presentation.fragments.signinup.events;

public interface SignUpBindingEvents {
    void onClickSignUp();
}
