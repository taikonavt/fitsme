package ru.fitsme.android.presentation.fragments.checkout;

public interface CheckoutBindingEvents {
    void goBack();
    void onClickMakeOrder();
}
